package org.villablanca;
import daw.com.Teclado;

/**
 * Clase con funciones que sirven para crear una <strong>cesta de la compra</strong>
 * la cual va ha tener 2 arrays, uno para los nombres de los productos (String) y otro para las cantidades de los mismos (int).
 * En la misma se van a poder aÃ±adir, modificar, eliminar y mostrar los productos, asÃ­ como la lista completa de ellos.
 * @author diegoc & Christian
 *
 */
public class CestaCompra {
	/***
	 * Atributos globales que representan una lista de la compra con la cantidad de productos (array int) y
	 * nombre de productos (array String). 
	 * Entero que contabiliza el numero de productos en la cesta. 
	 */
	private static int cantidadProductos[];
	private static String productos[];
	private static int numeroProductos;
	

	/**
	 * FunciÃ³n que intercambia los valores de ambos arrays
	 * @param productos Array de String a modificar
	 * @param cantidadProductos Array de int a modificar
	 * @param este Ã�ndice/PosiciÃ³n de ambos array a intercambiar por "int porEste"
	 * @param porEste Ã�ndice/PosiciÃ³n de ambos array que serÃ¡ intercambiado por "int este"
	 */
	public static void intercambiarValores(String[] productos, int[] cantidadProductos, int este, int porEste) {
		String aux1 = productos[este];
		int aux2 = cantidadProductos[este];
		
		productos[este] = productos[porEste];
		cantidadProductos[este] = cantidadProductos[porEste];
		
		productos[porEste] = aux1;
		cantidadProductos[porEste] = aux2;
	}
	
	/**
	 * FunciÃ³n que ordena alfabÃ©ticamente los arrays de la cesta de la compra
	 * @param productos Array de String de los nombres de los productos
	 * @param cantidadProductos Array de int de las cantidades que hay en la cesta de cada producto
	 */
	public static void ordenarAlfabeticamente(String[] productos, int[] cantidadProductos) {
		boolean hayDesorden = true;
		for(int i = 0; i < productos.length && i < cantidadProductos.length && hayDesorden; i++) {
			hayDesorden = false;
			for (int j = 1; j < productos.length; j++) {
				if (productos[j-1].compareToIgnoreCase(productos[j]) < 0) {
					intercambiarValores(productos, cantidadProductos, j-1, j);
				}
			}
		}
	}
	
	/**
	 * FunciÃ³n que muestra por pantalla la lista de la compra (previamente ordenada alfabeticamente)
	 * @param productos Array (String) de los nombres de los productos
	 * @param cantidadProductos Array (int) de las cantidades de cada producto
	 * @param numeroProductos Numero total de productos en la cesta
	 */
	public static void mostrarCesta(String[] productos, int[] cantidadProductos, int numeroProductos) {
		ordenarAlfabeticamente(productos, cantidadProductos);
		System.out.println("Lista total de la cesta:");
		for(int i = 0; i < productos.length && i < cantidadProductos.length; i++) {
			System.out.println(productos[i] + " -> Cantidad: " + cantidadProductos[i] + "uds.");
		}
	}

	/**
	 * FunciÃ³n que agrega un producto a la cesta (tanto el nombre como la cantidad)
	 * siempre y cuando el producto no exista en la cesta, si existe se llama a modificarProducto()
	 * @param productos Array de String de los nombres de los productos
	 * @param cantidadProductos Array de int de las cantidades de cada producto
	 * @param numeroProductos numero de productos totales en la cesta
	 * @return Devuelve la cantidad de productos totales en la cesta actualizado
	 */
	public static int addProducto(String[] productos, int[] cantidadProductos, int numeroProductos) {
		int cantidad=0;
		String producto = "";
		System.out.print("Â¿Cuantos productos vas a aÃ±adir? ");
		int add = Teclado.leerInt();
		for(int i = numeroProductos; i <= numeroProductos+add; i++) {
			System.out.print("Nombre del producto: ");
			producto = Teclado.leerString();
			do {
				System.out.print("Cantidad de " + producto + " a aÃ±adir: ");
				cantidad = Teclado.leerInt();
			} while (cantidad <= 0);
			productos[i] = producto;
			cantidadProductos[i] = cantidad;
		}
		return numeroProductos + add;
	}


	public static int eliminarProducto(int cantidad[], String productos[], int productoEliminar, int numeroProductos)  {
			if(numeroProductos<0) {
				System.out.println("no se puede eliminar ningun producto por que no hay productos en la cesta");
			}else {
				productos[productoEliminar]=productos[numeroProductos-1];
				cantidad[productoEliminar]=cantidad[numeroProductos-1];
			}
		return numeroProductos;
	}
	
	public static void MostrarMenu() {
		/**
		 * FunciÃ³n que imprime por pantalla el menÃº.
		 */
		System.out.println("Elige una opciÃ³n:\n"
				+ "1.- AÃ±adir producto\n"
				+ "2.- Eliminar producto\n"
				+ "3.- Calcular media productos\n"
				+ "4.- Salir Devuelve una opciÃ³n (De 1 a 4). Si la opciÃ³n es incorrecta, avisa y vuelve a pedir opciÃ³n.\n");
	}
	
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		
	}

}
